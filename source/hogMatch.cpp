#include "hogMatch.h"

void calculateHogFeatures(const Mat& img, vector<double>& hogVecNorm){
    
   Mat imgHog;
    Mat features = Mat(1, 3780, CV_32FC1);
    vector<float> descriptors;
    vector<double> hogVec;
    hogVecNorm.clear();

    
    resize(img, imgHog, Size(64,128), CV_INTER_LINEAR);
    
    HOGDescriptor hogDesriptor;
    hogDesriptor.compute(imgHog, descriptors);
    
    double vectorL2Norm = 0;
    double vectorL1Norm = 0;
    
    
    for(int z = 0; z < 3780; z++){
        //features.at<double>(0,z) = descriptors[z];
        //hogVec.push_back(descriptors[z]);
        vectorL2Norm += pow(descriptors[z],2);
        vectorL1Norm += descriptors[z];
        if(descriptors[z] < 0 )
            cout << "warning smaller than zero!\n";
    }
    
    //cout << "norm... ? " << vectorL2Norm << endl;

   /* if(maxVal == minVal){
        for(int z = 0; z < 3780; z++){
            hogVecNorm.push_back(0);
        }
        
        cout << "All the hog values for posture estimation are the same!!! \n";
    }
    */
    
    for(int z = 0; z < 3780; z++){
        //hogVecNorm.push_back(pow((descriptors[z] - minVal) / (maxVal - minVal),0.5) );
        //hogVecNorm.push_back(descriptors[z]/pow(vectorL2Norm,0.5));
        hogVecNorm.push_back(pow(descriptors[z]/vectorL1Norm,0.5));
    }
    
    
    //check the norm
    //due to runding it is e.g. 0.015702
    /*
    vectorL2Norm = 0;
    for(int z = 0; z < 3780; z++){
        vectorL2Norm += pow(hogVecNorm[z],2);
    }
    
    cout << "norm... ? " << vectorL2Norm << endl;
    */

}

///////
///
/////
/////////
//

double calcuclateHogDistance(const Mat& img1, const Mat& img2){
    
    //img1 and img2 are posture image (with shilouette mask and filled to square)
    
    double distance = 0;
    double partD = 0;
    
    vector<double> hogVecNorm1;
    vector<double> hogVecNorm2;
    
    
    calculateHogFeatures(img1, hogVecNorm1);
    calculateHogFeatures(img2, hogVecNorm2);

    
    for(int hit = 0; hit < 3780; hit++){
        //distance += hogVecNorm1[hit] * hogVecNorm2[hit];
        //distance += pow(abs(hogVecNorm1[hit] - hogVecNorm2[hit]),2);
        partD += hogVecNorm1[hit] * hogVecNorm2[hit];
       
    }
    //cout << "norm1 = " << norm1 << endl;
    //cout << "norm2 = " << norm1 << endl;


    //distance = 2 - 2 * partD;
    distance = partD;
    return distance;
    
}
