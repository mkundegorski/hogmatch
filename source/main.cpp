#include "hogMatch.h"



int main(int ac, char** av)
{
    /*
    Mat img1, img2;
    img1 = imread("./hogData/3c.png");
    img2 = imread("./hogData/1c.png");
    
    imshow("img1", img1);
    imshow("img2", img2);
    
    float result = calcuclateHogDistance(img1, img2);
    cout << result << "\n";
*/
    
    Mat fullRes(8,8,CV_32FC1);
    
    for(int i = 0; i < fullRes.cols; i++)
        for(int j = 0; j < fullRes.rows; j++){
            fullRes.at<float>(i,j) = (float) calcuclateHogDistance(imread("./hogData/" + to_string(i+1) + "c.png"), imread("./hogData/" + to_string(j+1) + "c.png"));
        }

    cout << fullRes << endl;
    
    cout << "Press any key to exit...\n";
    waitKey(0);
	return 0;

}
